#include "Sprite.h"
#include <iostream>

Sprite::Sprite()
{
	std::cout << "Sprite created" << std::endl;
}

int Sprite::GetComponentId()
{
	return GetId();
}

void Sprite::Render()
{
	std::cout << "Render Sprite: " << GetComponentId() << std::endl;
}
