#ifndef INPUTMANAGER_H__
#define INPUTMANAGER_H__

#include "ISystem.h"

class InputManager : public ISystem {

	friend class GameEngine;

	InputManager();
	~InputManager();
	InputManager(InputManager& const) = delete;
	InputManager& operator=(InputManager& const) = delete;

public:
	static InputManager& Instance();

protected:
	void Initialize();
	void Update();
};

#endif
