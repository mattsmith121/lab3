#ifndef RENDERSYSTEM_H__
#define RENDERSYSTEM_H__

#include "ISystem.h"
#include <string>
#include <list>

class RenderSystem : public ISystem {

	class IRenderable;
	friend class GameEngine;

	std::list<IRenderable*> renderComponents;
	std::string name;
	int width;
	int height;
	bool fullscreen;

	RenderSystem();
	~RenderSystem();
	void LoadSettings();
	RenderSystem(RenderSystem& const) = delete;
	RenderSystem& operator=(RenderSystem& const) = delete;

public:
	static RenderSystem& Instance();
	void AddRenderable(IRenderable* component);
	void RemoveRenderable(IRenderable* component);

protected:
	void Update();
	void Initialize();
};

#endif