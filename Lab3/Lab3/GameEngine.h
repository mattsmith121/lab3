#ifndef GAMEENGINE_H__
#define GAMEENGINE_H__

class GameEngine {
	GameEngine();
	~GameEngine();

public:
	static GameEngine& Instance();
	void Initialize();
	void GameLoop();
};

#endif