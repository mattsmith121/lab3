#include "GameEngine.h"
#include "RenderSystem.h"
#include "FileSystem.h"
#include "InputManager.h"
#include "AssetManager.h"
#include "GameObjectManager.h"
#include "json.hpp"
#include <fstream>
#include <string>
#include <chrono>

GameEngine::GameEngine() {
	std::cout << "GameEngine Created" << std::endl;
}

GameEngine::~GameEngine() {
	std::cout << "GameEngine Destroyed" << std::endl;
}

GameEngine& GameEngine::Instance() {
	static GameEngine instance;
	return instance;
}

void GameEngine::Initialize() {
	// Initialize other objects
	RenderSystem::Instance().Initialize();
	FileSystem::Instance().Initialize();
	InputManager::Instance().Initialize();
	AssetManager::Instance().Initialize();
	GameObjectManager::Instance().Initialize();

	std::cout << "GameEngine Initialized" << std::endl;
}

void GameEngine::GameLoop() {

	std::cout << "GameEngine GameLoop" << std::endl;

	// Keep track of time
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed;
	// Run for ~10 seconds
	do {
		RenderSystem::Instance().Update();
		FileSystem::Instance().Update();
		InputManager::Instance().Update();
		AssetManager::Instance().Update();
		GameObjectManager::Instance().Update();
		
		// Update time
		elapsed = std::chrono::system_clock::now() - start;
	} while (elapsed.count() < 10.0);
}