#pragma once
#ifndef _SPRITE_H_
#define _SPRITE_H_

#include "Component.h"
#include "IRenderable.h"

class Sprite : public Component, public IRenderable
{
public:
	Sprite();
	int GetComponentId();
protected:
	void Render();
};

#endif // !_SPRITE_H_