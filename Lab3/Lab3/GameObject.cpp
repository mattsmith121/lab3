#include "GameObject.h"
#include <iostream>

GameObject::GameObject() {
	std::cout << "GameObject Created" << std::endl;
}

GameObject::~GameObject() {
	// Delete all the components
	for (auto it : components) {
		delete(it.second);
	}
	components.clear();
	std::cout << "GameObject Destroyed" << std::endl;
}

void GameObject::Initialize() {
	components = std::map<std::string, Component*>();
	std::cout << "GameObject Initialized" << std::endl;
}

void GameObject::Update() {
	std::cout << "GameObject Update" << std::endl;
	// Call update on components
	for (auto it : components) {
		it.second->Update();
	}
}

void GameObject::AddComponent(Component* component) {
	components.emplace(component->GetName(), component);
}

void GameObject::RemoveComponent(Component* component) {
	if (components.find(component->GetName()) == components.end()) {
		return;
	}

	components.erase(component->GetName());
	delete(component);
}