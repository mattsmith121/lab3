#pragma once
#ifndef _ISYSTEM_H_
#define _ISYSTEM_H_

class ISystem
{
	friend class GameEngine;

protected:
	virtual void Initialize() = 0;
	virtual void Update() = 0;
};

#endif // !_ISYSTEM_H_
