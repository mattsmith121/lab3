#include "GameObjectManager.h"
#include "GameObject.h"
#include <iostream>
#include <list>

GameObjectManager::GameObjectManager() {
	std::cout << "GameObjectManager Created" << std::endl;
}

GameObjectManager::~GameObjectManager() {
	// Delete all the Game Objects
	while (gameObjects.size() > 0) {
		delete(gameObjects.back());
		gameObjects.pop_back();
	}
	std::cout << "GameObjectManager Destroyed" << std::endl;
}

void GameObjectManager::Initialize(){
	gameObjects = std::list<GameObject*>();
	std::cout << "GameObjectManager Initialized" << std::endl;
}

GameObjectManager& GameObjectManager::Instance()
{
	static GameObjectManager instance;
	return instance;
}

void GameObjectManager::AddGameObject(GameObject* gameObject) {
	gameObjects.push_back(gameObject);
}

void GameObjectManager::RemoveGameObject(GameObject* gameObject) {
	// Remove_if will call the destructor and reduce the size of the list
	gameObjects.remove_if([gameObject](GameObject* go) {
		return go->GetName() == gameObject->GetName();
	});
}

GameObject* GameObjectManager::FindGameObjectById(int id)
{
	for (auto it : gameObjects) {
		if (it->GetId() == id) {
			return it;
		}
	}
}

void GameObjectManager::Update() {
	std::cout << "GameObjectManager Update" << std::endl;
	// Call update on GameObjects
	for (std::list<GameObject*>::iterator it = gameObjects.begin();
		it != gameObjects.end(); it++) {
		(*it)->Update();
	}
}