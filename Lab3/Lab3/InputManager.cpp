#include "InputManager.h"
#include <iostream>

InputManager::InputManager(){
	std::cout << "InputManager Created" << std::endl;
}

InputManager::~InputManager(){
	std::cout << "InputManager Destroyed" << std::endl;
}

InputManager& InputManager::Instance()
{
	static InputManager instance;
	return instance;
}

void InputManager::Initialize() {
	std::cout << "InputManager Initialized" << std::endl;
}

void InputManager::Update() {
	std::cout << "InputManager Update" << std::endl;
}
