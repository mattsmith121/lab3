#ifndef COMPONENT_H__
#define COMPONENT_H__

#include "Object.h"

class Component : public Object {
public:
	void Initialize();
	virtual int GetComponentId() = 0;
	void Update();
	virtual ~Component() = 0;

protected:
	Component();
};

#endif