#ifndef GAMEOBJECTMANAGER_H__
#define GAMEOBJECTMANAGER_H__

#include <list>
#include "ISystem.h"

class GameObject;

class GameObjectManager : public ISystem {

	friend class GameEngine;
	
	std::list<GameObject*> gameObjects;

	GameObjectManager();
	~GameObjectManager();
	GameObjectManager(GameObjectManager& const) = delete;
	GameObjectManager& operator=(GameObjectManager& const) = delete;

public:
	static GameObjectManager& Instance();
	void AddGameObject(GameObject* gameObject);
	void RemoveGameObject(GameObject* gameObject);
	GameObject* FindGameObjectById(int id);

protected:
	void Initialize();
	void Update();
};

#endif
