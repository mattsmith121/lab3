#include "IRenderable.h"
#include <iostream>

IRenderable::IRenderable()
{
	std::cout << "IRenderable created" << std::endl;
}

IRenderable::~IRenderable()
{
	std::cout << "IRenderable destroyed" << std::endl;
}
