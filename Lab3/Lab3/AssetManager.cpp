#include "AssetManager.h"
#include "Asset.h"
#include <iostream>

AssetManager::AssetManager() {
	std::cout << "AssetManager Created" << std::endl;
}

AssetManager::~AssetManager() {
	std::cout << "AssetManager Destroyed" << std::endl;
}

AssetManager& AssetManager::Instance()
{
	static AssetManager instance;
	return instance;
}

void AssetManager::AddAsset(Asset* component)
{
	assets.push_back(component);
}

void AssetManager::RemoveAsset(Asset* component)
{
	assets.remove(component);
}

void AssetManager::Initialize() {
	std::cout << "AssetManager Initialized" << std::endl;
}

void AssetManager::Update() {
	std::cout << "AssetManager Update" << std::endl;
}

