#ifndef OBJECT_H__
#define OBJECT_H__

#include <string>

class Object {
	bool initialized;
	std::string name;
	int id;

public:
	std::string& GetName();
	bool IsInitialized();
	int GetId();
	void Initialize();

protected:
	Object();
	~Object();
};

#endif
