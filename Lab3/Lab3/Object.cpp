#include "Object.h"
#include <iostream>
#include <string>
#include <chrono>
#include <stdlib.h>

Object::Object() {
	initialized = false;
	name = "";
	id = 0;
	std::cout << "Object Created" << std::endl;
}

Object::~Object() {
	std::cout << "Object Destroyed" << std::endl;
}

bool Object::IsInitialized() {
	return initialized;
}

std::string& Object::GetName() {
	return name;
}

int Object::GetId() {
	return id;
}

void Object::Initialize() {
	initialized = true;

	// Get identifier
	srand(std::chrono::system_clock::now().time_since_epoch().count());
	id = rand(); // Random id between 0 and RAND_MAX

	std::cout << "Object Initialized" << std::endl;
}