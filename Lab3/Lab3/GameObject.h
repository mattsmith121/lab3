#ifndef GAMEOBJECT_H__
#define GAMEOBJECT_H__

#include <map>
#include <string>
#include "Component.h"

class GameObject : public Object {
	std::map<std::string, Component*> components;
public:
	GameObject();
	~GameObject();
	void Initialize();
	void AddComponent(Component* component);
	void RemoveComponent(Component* component);
	void Update();
};

#endif