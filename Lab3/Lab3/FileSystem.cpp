#include "FileSystem.h"
#include <iostream>

FileSystem::FileSystem() {
	std::cout << "FileSystem Created" << std::endl;
}

FileSystem::~FileSystem() {
	std::cout << "FileSystem Destroyed" << std::endl;
}

FileSystem& FileSystem::Instance()
{
	static FileSystem instance;
	return instance;
}

void FileSystem::Initialize() {
	std::cout << "FileSystem Initialized" << std::endl;
}

void FileSystem::Update() {
	std::cout << "FileSystem Update" << std::endl;
}

void FileSystem::Load(std::string fileName)
{
	std::cout << "Loading FileSystem settings..." << std::endl;
}

