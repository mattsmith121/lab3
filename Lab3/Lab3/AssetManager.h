#ifndef ASSETMANAGER_H__
#define ASSETMANAGER_H__

#include "ISystem.h"
#include <list>

class AssetManager {

	friend class GameEngine;
	class Asset;

	std::list<Asset*> assets;

	AssetManager();
	~AssetManager();
	AssetManager(AssetManager& const) = delete;
	AssetManager& operator=(AssetManager& const) = delete;

public:
	static AssetManager& Instance();
	void AddAsset(Asset* component);
	void RemoveAsset(Asset* component);

protected:
	void Initialize();
	void Update();
};

#endif
