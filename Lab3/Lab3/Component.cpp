#include "Component.h"
#include <iostream>

Component::Component() {
	std::cout << "Component Created" << std::endl;
}

Component::~Component() {
	std::cout << "Component Destroyed" << std::endl;
}

void Component::Initialize(){
	Object::Initialize();
	std::cout << "Component Initialized" << std::endl;
}

void Component::Update() {
	std::cout << "Component update" << std::endl;
}