#include "RenderSystem.h"
#include "json.hpp"
#include <iostream>
#include <fstream>

RenderSystem::RenderSystem() {
	name = "";
	width = 0;
	height = 0;
	fullscreen = false;
	std::cout << "RenderSystem Created" << std::endl;
}

RenderSystem::~RenderSystem() {
	std::cout << "RenderSystem Destroyed" << std::endl;
}

RenderSystem& RenderSystem::Instance()
{
	static RenderSystem instance;
	return instance;
}

void RenderSystem::Initialize()
{
	LoadSettings();
}

void RenderSystem::LoadSettings()
{
	// Load in the GameSettings.json
	std::ifstream inputStreamSettings("RenderSystem.json");
	std::string settingsString((std::istreambuf_iterator<char>(inputStreamSettings)),
		std::istreambuf_iterator<char>());

	json::JSON renderSystem = json::JSON::Load(settingsString);

	// Load in render system settings
	if (renderSystem.hasKey("name")) {
		name = renderSystem["name"].ToString();
	}

	if (renderSystem.hasKey("width")) {
		width = renderSystem["width"].ToInt();
	}

	if (renderSystem.hasKey("height")) {
		height = renderSystem["height"].ToInt();
	}

	if (renderSystem.hasKey("fullscreen")) {
		fullscreen = renderSystem["fullscreen"].ToBool();
	}

	std::cout << "Render System Settings:" << std::endl;
	std::cout << "name: "  << name << std::endl;
	std::cout << "width: " << width << std::endl;
	std::cout << "height: " << height << std::endl;
	std::cout << "fullscreen: " << (fullscreen ? "true" : "false") << std::endl;

	std::cout << "Render System Initialized" << std::endl;
}

void RenderSystem::AddRenderable(IRenderable* component)
{
	renderComponents.push_back(component);
}

void RenderSystem::RemoveRenderable(IRenderable* component)
{
	// IRenderable has no discerning characteristics,
	// so just use remove for now
	renderComponents.remove(component);
}

void RenderSystem::Update() {
	std::cout << "RenderSystem Update" << std::endl;
}
