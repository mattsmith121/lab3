#ifndef FILESYSTEM_H__
#define FILESYSTEM_H__

#include "ISystem.h"
#include <string>

class FileSystem : public ISystem {

	friend class GameEngine;

	FileSystem();
	~FileSystem();
	FileSystem(FileSystem& const) = delete;
	FileSystem& operator=(FileSystem& const) = delete;

public:
	static FileSystem& Instance();

protected:
	void Initialize();
	void Update();
	void Load(std::string fileName);
};

#endif
